package CreateLead;


import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase {
	@Before
	public void beforeMethod() {
		startApp("chrome", "http://leaftaps.com/opentaps");
	  }

	
	public void testcaseName(Scenario sc) {
		System.out.println(sc.getName());
		System.out.println(sc.getId());
		
	}

	@After
	public void testcasestatus(Scenario sc) {
		System.out.println(sc.getStatus());
	}
	
	 public void afterMethod() {
		  close();
	  }

}
