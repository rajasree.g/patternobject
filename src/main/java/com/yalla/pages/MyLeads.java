package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MyLeads extends Annotations{
	
	public MyLeads() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLead;
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement eleFindLead;
	
	public CreateLead ClickCreateLeads() {
		click(eleCreateLead);
		return new CreateLead();
		
	}
	
	public FindLeads clickFindLeads() {
		click(eleFindLead);
		return new FindLeads();
	}

}
