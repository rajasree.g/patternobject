package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLead extends Annotations{

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using="createLeadForm_companyName") WebElement elecompanyname;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement elefirstname;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement elelastname;
	@FindBy(how=How.NAME, using="submitButton") WebElement elesubmit;
	
	public CreateLead enterCompanyName(String CName) {
	elecompanyname.sendKeys(CName);
	return this;
	}
	
	public CreateLead enterFirstName(String FName) {
		elefirstname.sendKeys(FName);
		return this;
	}
	
	public CreateLead enterLastName(String LName) {
		elelastname.sendKeys(LName);
		return this;
	}
	
	public ViewLead clickCreateLead() {
		click(elesubmit);
		return new ViewLead();
	}
}

