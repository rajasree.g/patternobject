package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MyhomePage extends Annotations {


public  MyhomePage(){
	PageFactory.initElements(driver, this);
	
}
@FindBy(how=How.LINK_TEXT, using="Leads") WebElement eleleads;

public MyLeads ClickLeads() {
	click(eleleads);
	return new MyLeads();
}

}