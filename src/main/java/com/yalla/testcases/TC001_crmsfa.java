package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.HomePage;
import com.yalla.testng.api.base.Annotations;

public class TC001_crmsfa extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testcaseDec = "To create a new lead";
		author = "KEERTHANA";
		category = "smoke";
}
	@Test
	public void crmsfa() {
		new HomePage()
		.clickCRMSFA();
	}
}
