package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.CreateLead;
import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.pages.MyLeads;
import com.yalla.pages.MyhomePage;
import com.yalla.testng.api.base.Annotations;

public class TC001_CreateLead extends Annotations {


	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testcaseDec = "To create a new lead";
		author = "KEERTHANA";
		category = "smoke";
		excelFileName = "CreateLead";
	} 

	
		@Test(dataProvider="fetchData")
public void CreateLeadTest(String uName, String pwd, String CompanyName, String FirstName, String LastName) {
			new LoginPage()
			.enterUserName(uName)
			.enterPassWord(pwd) 
			.clickLogin();
			new HomePage()
			.clickCRMSFA();
			new MyhomePage()
			.ClickLeads();
			new MyLeads()
			.ClickCreateLeads();
			new CreateLead()
		.enterCompanyName(CompanyName)
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.clickCreateLead();
	}
}
